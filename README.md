# Tugas 1 Html, css, Javascript

* git clone https://gitlab.com/bungendang/tugas1
* npm install
* grunt

# Instruction
1. Buatlah 3 buah form pada file index.html, masing masing form terdapat field input dan tombol submit.
2. Pada Navigation Menu, jika salah satu menu di klik maka form yang bersangkutan akan ditampilkan dan form yang lain akan disembunyikan.
3. Hasil inputan akan ditampilkan pada div tag ber id #output.

# Clue
1. Beri 'display : none' pada css class form gaji dan form biodata, 'display:block' pada form contact.
2. Beri class active dan hilangkan class active pada menu yang bersangkutan, gunakan function addClass dan removeClass pada javascript.
3. Gunakan perintah innerHTML untuk mengganti output dari div ber id #output.

